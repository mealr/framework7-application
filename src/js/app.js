import $ from 'dom7';
import Framework7, { getDevice } from 'framework7/bundle';

// Import F7 Styles
import 'framework7/framework7-bundle.css';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.scss';
// Import Cordova APIs
import cordovaApp from './cordova-app.js';



// Import Routes
import routes from './routes.js';
// Import Store
import store from './store.js';

// Import main app component
import App from '../app.f7';

// import App from '../views/splash.f7';

// import jQuery from "jquery";
// // jquery inits
// window.jQuery = jQuery;
// window.$ = jQuery;

var device = getDevice();
var app = new Framework7({
  name: 'Mealr', // App name
  theme: 'auto', // Automatic theme detection
  el: '#app', // App root element
  component: App, // App main component
  id: 'com.themealr.user', // App bundle ID
  // App store
  store: store,
  // App routes
  routes: routes,
  // Register service worker (only on production build)
  serviceWorker: process.env.NODE_ENV ==='production' ? {
    path: '/service-worker.js',
  } : {},

  // Input settings
  input: {
    scrollIntoViewOnFocus: true,
    scrollIntoViewCentered: true,
  },
  // Cordova Statusbar settings
  statusbar: {
    iosOverlaysWebView: false,
    androidOverlaysWebView: false,
  },
  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);
      }
    },
  },
});

// if(device.cordova){
//
//   document.addEventListener("deviceready", function() {
//     window.open = cordova.InAppBrowser.open;
//   }, false);
//
// }
