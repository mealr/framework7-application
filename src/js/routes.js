

import SplashPage from '../views/splash.f7';
import LoginPage from '../views/auth/login.f7';
import VerifyOTPPage from '../views/auth/verify_otp.f7';
import SignUpPage from '../views/auth/signup.f7';
import AddressPage from '../views/address/addresses.f7';
import AddAddressPage from '../views/address/add_address.f7';
import HomePage from '../views/home/home.f7';
import SubscriptionPage from '../views/home/home.f7';



// import DynamicRoutePage from '../pages/dynamic-route.f7';
// import RequestAndLoad from '../pages/request-and-load.f7';
import NotFoundPage from '../pages/404.f7';

// import jQuery from "jquery";
// // jquery inits
// window.jQuery = jQuery;
// window.$ = jQuery;



var routes = [
  {
    path: '/',
    component: SplashPage,
    name: "splash",
    options: {
      transition: 'f7-circle',
    },
    // name: "splash",
    on: {
      pageAfterIn: function (e, page) {
        // do something after page gets into the view
        // setTimeout(()=>{

          if(!localStorage.getItem("userData"))
            page.app.views.main.router.navigate({name: "home"})
          else
            page.app.views.main.router.navigate({name: "home"})

        // },3000);
      },
      pageInit: function (e, page) {
        // do something when page initialized
      },
    },
    async: function ({ router, to, resolve }){
      console.log("hi in async")
    }
  },
  {
    path: '/auth/login/',
    component: LoginPage,
    name: "login",
    options: {
      transition: 'f7-circle',
    },
    keepAlive: true
  },
  {
    path: '/auth/verify-otp/',
    component: VerifyOTPPage,
    name: "verify-otp",
    options: {
      transition: 'f7-circle',
    },
  },
  {
    path: '/auth/signup/',
    component: SignUpPage,
    name: "signup",
    options: {
      transition: 'f7-circle',
    },
  },
  {
    path: '/home/address',
    component: AddressPage,
    name: "addresses",
    options: {
      transition: 'f7-circle',
    },
  },
  {
    path: '/home/address/create',
    component: AddAddressPage,
    name: "address_add",
    options: {
      transition: 'f7-fade',
    }
  },
  {
    path: '/home',
    name: "home",
    // component: HomePage,
    // url: "../views/home/index.html",
    options: {
      transition: 'f7-fade',
    },
    tabs: [
      {
        path: "/",
        id: "home-tab",
        componentUrl: "../views/home/index.html",
        name: "home-tab",
      },
      {
        path: "/subscription",
        id: "subscription-tab",
        componentUrl: "../views/home/index.html",
        name: "subscription-tab",
      }
    ]
  },

  /*{
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            props: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },*/
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
